/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CiilId.CRUD;

import Cill_id.DBConection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 *
 */
public class CRUD_Pesan {
    private Connection koneksi;
    private PreparedStatement ps;
    private DBConection kdb = new DBConection();
    //private String id_studio, nomor_studio, judul_film;
    
    public CRUD_Pesan(){
    
    }
    
    public void comboDataBioskop(DefaultComboBoxModel model){
        PreparedStatement ps = null;
        ResultSet rs = null;
       
        String strSql="SELECT * FROM bioskop";
       
        try {
            kdb.bukaKoneksi();
            koneksi = kdb.getConn();
            ps = koneksi.prepareStatement(strSql);
            rs = ps.executeQuery();
           
            while (rs.next()){
                //model.addElement(rs.getString(2));
                if(model.getIndexOf(rs.getString(2)) == -1 ) {
                    model.addElement(rs.getString(2));
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Terjadi Kesalahan : " +e);
        }
    }
    
    public void comboDataFilmSpesifik(DefaultComboBoxModel model, String index){
        PreparedStatement ps = null;
        ResultSet rs = null;
       
        String strSql="SELECT * FROM film  ";
       
        try {
            kdb.bukaKoneksi();
            koneksi = kdb.getConn();
            ps = koneksi.prepareStatement(strSql);
            rs = ps.executeQuery();
           
            while (rs.next()){
                //model.addElement(rs.getString(2));
                if(model.getIndexOf(rs.getString(3)) == -1 ) {
                    model.addElement(rs.getString(3));
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Terjadi Kesalahan : " +e);
        }
    }
    
    public void labelDataIdStudio(JLabel label, String jam_tayang, String judul){
        PreparedStatement ps = null;
        ResultSet rs = null;
       
        String strSql="SELECT * FROM studio WHERE jam_tayang = '"+jam_tayang+"' AND judul = '"+judul+"' ";
        
        try {
            kdb.bukaKoneksi();
            koneksi = kdb.getConn();
            ps = koneksi.prepareStatement(strSql);
            rs = ps.executeQuery();
           
            while (rs.next()){
                label.setText(rs.getString(2));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Terjadi Kesalahan : " +e);
        }
    }
    
    public void labelDataHarga(JLabel label, String judul, String jam_tayang){
        PreparedStatement ps = null;
        ResultSet rs = null;
       
        String strSql="SELECT * FROM film WHERE judul = '"+judul+"' ";
//        String strSql="SELECT nama_bioskop ='"+nama_bioskop+"' ,judul ='"+judul+"',jam_tayang '"+jam_tayang+"' FROM film INNER JOIN studio USING (judul = '"+judul+"')";
        //String strSql= "SELECT `nama_bioskop` ,`judul`,`jam_tayang` FROM `film` INNER JOIN `studio` USING (judul) WHERE `nama_bioskop`='"+nama_bioskop+"' AND `judul`='"+judul+"' AND `jam_tayang`='"+jam_tayang+"' ";
        //String strSql="SELECT 'nama_bioskop', 'judul', 'harga', 'jam_tayang' FROM film INNER JOIN studio USING (judul) WHERE nama_bioskop = '"+nama_bioskop+"' AND judul = '"+judul+"' AND harga= '"+harga+"' AND jam_tayang= '"+jam_tayang+"' ";
        try {
            kdb.bukaKoneksi();
            koneksi = kdb.getConn();
            ps = koneksi.prepareStatement(strSql);
            rs = ps.executeQuery();
           
            while (rs.next()){
                label.setText(rs.getString(2));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Terjadi Kesalahan : " +e);
        }
    }
    
    public void labelDataWaktuMulai(JLabel label, String nama_bioskop, String judul){
        PreparedStatement ps = null;
        ResultSet rs = null;
       
        String strSql="SELECT * FROM film WHERE nama_bioskop = '"+nama_bioskop+"' AND judul = '"+judul+"' ";
        
        try {
            kdb.bukaKoneksi();
            koneksi = kdb.getConn();
            ps = koneksi.prepareStatement(strSql);
            rs = ps.executeQuery();
           
            while (rs.next()){
                label.setText(rs.getString(7));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Terjadi Kesalahan : " +e);
        }
    }
    
    public void labelDataWaktuSelesai(JLabel label, String nama_bioskop, String judul){
        PreparedStatement ps = null;
        ResultSet rs = null;
       
        String strSql="SELECT * FROM film WHERE nama_bioskop = '"+nama_bioskop+"' AND judul = '"+judul+"' ";
        
        try {
            kdb.bukaKoneksi();
            koneksi = kdb.getConn();
            ps = koneksi.prepareStatement(strSql);
            rs = ps.executeQuery();
           
            while (rs.next()){
                label.setText(rs.getString(8));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Terjadi Kesalahan : " +e);
        }
    }
    
    public void labelDataTanggal(JLabel label,String judul){
        PreparedStatement ps = null;
        ResultSet rs = null;
       
        String strSql="SELECT * FROM film WHERE  judul = '"+judul+"' ";
        
        try {
            kdb.bukaKoneksi();
            koneksi = kdb.getConn();
            ps = koneksi.prepareStatement(strSql);
            rs = ps.executeQuery();
           
            while (rs.next()){
                label.setText(rs.getString(6));
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Terjadi Kesalahan : " +e);
        }
    }
    
    public void comboDataFilm(DefaultComboBoxModel model){
        PreparedStatement ps = null;
        ResultSet rs = null;
       
        String strSql="SELECT * FROM film";
       
        try {
            kdb.bukaKoneksi();
            koneksi = kdb.getConn();
            ps = koneksi.prepareStatement(strSql);
            rs = ps.executeQuery();
           
            while (rs.next()){
                //model.addElement(rs.getString(2));
                if(model.getIndexOf(rs.getString(3)) == -1 ) {
                    model.addElement(rs.getString(3));
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Terjadi Kesalahan : " +e);
        }
    }
    
    public void comboDataJam(DefaultComboBoxModel model){
        PreparedStatement ps = null;
        ResultSet rs = null;
       
        String strSql="SELECT * FROM studio";
       
        try {
            kdb.bukaKoneksi();
            koneksi = kdb.getConn();
            ps = koneksi.prepareStatement(strSql);
            rs = ps.executeQuery();
           
            while (rs.next()){
                //model.addElement(rs.getString(2));
                if(model.getIndexOf(rs.getString(4)) == -1 ) {
                    model.addElement(rs.getString(4));
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Terjadi Kesalahan : " +e);
        }
    }
    
    public void comboDataStudio(DefaultComboBoxModel model){
        PreparedStatement ps = null;
        ResultSet rs = null;
       
        String strSql="SELECT * FROM studio";
       
        try {
            kdb.bukaKoneksi();
            koneksi = kdb.getConn();
            ps = koneksi.prepareStatement(strSql);
            rs = ps.executeQuery();
           
            while (rs.next()){
                //model.addElement(rs.getString(2));
                if(model.getIndexOf(rs.getString(2)) == -1 ) {
                    model.addElement(rs.getString(2));
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"Terjadi Kesalahan : " +e);
        }
    }

}
