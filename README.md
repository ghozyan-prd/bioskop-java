**APLIKASI PEMESANAN TIKET BIOSKOP - JAVA** <br>
**Ghozyan Hilman Pradana - Nur Isnaini**


**Aplikasi Pemesanan Tiket Film Bioskop Berbasis Desktop dengan Java :** <br>
> **Penjelasan Umum :** Aplikasi pemesanan tiket bioskop Cill ID adalah aplikasi berbasis Dekstop yang mampu menginputkan pesanan 
> tiket dan dapat mengedit stusio, film dan juga mengetahui laporan pemesanan setiap harinya. Aplikasi ini 
> mempunyai dua log in yaitu log in user dan log in admin. Untuk log in user aplikasi ini hanya dapat 
> memesan tiket sedangkan untuk log in admin aplikasi ini dapat melakukan edit studio, edit jadwal, 
> dan juga menerima laporan pemesanan setiap hari guna untuk mengetahui apakah kursi dan studio 
> tesebut sudah penuh atau belum, sehingga jika sudah tayang maka admin dapat mereset kursi 
> agar dapat dipesan kembali dengan jadwal film yang baru. Berikut langkah-lang 
> menggunakan aplikasi Cill ID:
<br>


**Cara menggunakan Aplikasi Cill_id**
> * [ Pastikan Laptop atau Pc anda sudah terinstall java jdk]
> * [ Clone Aplikasi Bioskop Cill_id ini ke dalam Pc anda]
> * [ Persipakan library yang di gunakan, seperti Mysql Connector, JdateCooser, dan I-report]
> * [ Import database nya dan lakukan setting koneksi di dalam file Koneksi.java ] 

**Menu Pada  Aplikasi Cill ID<br>**
Log in dapat digunakan oleh user .  jika log in user maka klik icon user, lalu masukan username dengan nama user dan sandi user<br>

*  **User**<br>![datei](/to/Picture1.png) ![datei](/to/Picture2.png) <br> <br>

*  Setelah user login maka user dapat menginputkan pesanan tiket yang diinginkan oleh pembeli.<br>![datei](/to/Picture3.png) <br> <br>

*  Masukkan judul film, dan waktu sesuai keinginan pembeli. Lalu klik refresh sebelum memilih no kursi yang diinginkan untuk memunculkan no studio dan harga film.  setelah itu maka klik no kursi yang diinginkan dan klik refresh kembali untuk memunculkan warna abu-abu pada kursi yang telah dipilih untuk menandakan kursi sudah dipilih dan tidak bisa dipesan kembali. Setelah itu klik pesan dan akan muncul tampilan transaksi berhasil seperti gambar dibawah ini. Lalu klik oke  maka secara langsung data tersebut mengeprint dengan sendirinya dan  halaman pindah ke form login dan muncul print <br>
![datei](/to/Picture4.png) ![datei](/to/Picture5.png) <br> <br>

*  **Admin** <br>Jika log in admin maka klik admin lalu masukkan  username admin dan sandi admin seperti gambar dibawah ini <br>
![datei](/to/Picture6.png) <br><br>

*  Setelah log in admin, maka admin dapat mengedit studio, edit film, dan juga  mengetahui laporan pemesan film. Jika admin melakukan edit film maka klik edit film seperti gambar dibawah ini.<br>
![datei](/to/Picture7.png) <br><br>

*  Setelah itu masukkan tanggal, judul film, genre, sutradara, durasi dan harga film. Lalu klik simpan pada kolom bawah itu maka akan muncul tanggal dan jadwal film yang telah diinputkan tadi. <br>
![datei](/to/Picture8.png) ![datei](/to/Picture9.png) <br><br>

*  Admin dapat menghapus film yang sudah tayang. Klik data yang akan dihapus lalu setelah itu klik hapus maka data film yang dipilih akan hilang. <br>
![datei](/to/Picture10.png) ![datei](/to/Picture11.png) <br><br>

*   Admin dapat mengedit studio dengan cara klik tabel studio<br>
![datei](/to/Picture12.png) <br><br>

*   Lalu klik film yang akan diedit, setelah itu klik waktu dan juga studio, setalah itu klik simpan maka film yang sudah dipilih dan studio yang dipilih tadi akan muncul pada tabel dibawah seperti gambar berikut. <br>
![datei](/to/Picture13.png) <br><br>
*   Jika kursi sudah penuh, maka admin dapat mereset kursi tersebut dengan tekan resert kursi . Setelah itu, kursi dapat dipesan kembali (tampilan kursi tidak berwarna abu - abu lagi ) <br>

*   Admin dapat melihat  data-data pesanan tiket setiap harinya dengan cara klik tabel laporan<br>
![datei](/to/Picture14.png) <br><br>
*   Setelah itu akan muncul data-data pesanan setiap harinya<br>
![datei](/to/Picture15.png) <br><br>
*   Lalu klik cetak report untuk mengeprint data tersebut.<br>
![datei](/to/Picture16.png) <br><br>